package main

import (
	"bufio"
	"io"
	"os/exec"
	"strings"
)

type Scanner struct {
	CommandLine string
	sout, serr  io.ReadCloser
	ltmp        int
	rout, rerr  *bufio.Reader
	command     *exec.Cmd
}

func NewScanner(cmd string, argstr []string) *Scanner {
	s := new(Scanner)
	s.command = exec.Command(cmd, argstr...)
	s.sout, _ = s.command.StdoutPipe()
	s.serr, _ = s.command.StderrPipe()
	s.rout = bufio.NewReader(s.sout)
	s.rerr = bufio.NewReader(s.serr)
	s.CommandLine = cmd + " " + strings.Join(argstr, " ")
	return s
}

func (s *Scanner) Start() {
	s.command.Start()
}

func (s *Scanner) ReadLine() (string, error) {
	return s.rout.ReadString('\n')
}
