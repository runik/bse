package main

import (
	"github.com/yuin/gopher-lua"
)

func NewVm() *lua.LState {
	L := lua.NewState()
	L.SetGlobal("command", L.NewFunction(LCommand))
	L.SetGlobal("match", L.NewFunction(LMatch))
	L.SetGlobal("asynctask", L.NewFunction(LAsyncTask))
	L.SetGlobal("synctask", L.NewFunction(LSyncTask))
	L.SetGlobal("mkdir", L.NewFunction(LMkdir))
	L.SetGlobal("writelist", L.NewFunction(LWriteList))
	defer L.Close()
	return L
}

func RunVmTask(args *lua.LTable, L *lua.LState) lua.LValue {
	if err := L.CallByParam(lua.P{
		Fn:      L.GetGlobal("run"),
		NRet:    1,
		Protect: true,
	}, args); err != nil {
		panic(err)
	}
	v := L.Get(-1) // returned value
	L.Pop(1)       // remove received value
	return v
}
