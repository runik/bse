package main

import (
	"bufio"
	"fmt"
	"github.com/yuin/gopher-lua"
	"os"
	"strings"
)

func execSingle(cmdline string, fl *os.File, L *lua.LState) {
	execTask(cmdline, fl, L)
}

func execBatch(cmdline string, fl *os.File, fb *os.File, L *lua.LState) {
	scanner := bufio.NewScanner(fb)
	for scanner.Scan() {
		c := strings.Replace(cmdline, "^LINE^", scanner.Text(), 1)
		execTask(c, fl, L)
	}
}

func execTask(cmdline string, fl *os.File, L *lua.LState) {
	task := NewTask(cmdline)
	task.Scanner.Start()
	for {
		line, err := task.Scanner.ReadLine()

		if err != nil {
			break
		}
		if fl != nil {
			fl.WriteString(line)
		}

		if err := L.CallByParam(lua.P{
			Fn:      L.GetGlobal("parse"),
			NRet:    0,
			Protect: true,
		}, lua.LString(line)); err != nil {
			panic(err)
		}
	}
}

/*
  Run a system command. If a batch file is included then
  it will run the command against each line in the batch
  file, replacing the ^LINE^ token with the line.

    command("nmap 127.0.0.1", "verbose/stdout.log")
    command("nmap ^LINE^", "verbose/stdout.log", "batch/listing.lst")
*/
func LCommand(L *lua.LState) int {
	cmdline := L.ToString(1)   /* get argument */
	logfile := L.ToString(2)   /* get argument */
	batchfile := L.ToString(3) /* get argument */

	var fl *os.File = nil
	var fb *os.File = nil

	if len(logfile) > 0 {
		fl, _ = os.Create(logfile)
		defer fl.Close()
	}

	if len(batchfile) > 0 {
		fb, err := os.Open(batchfile)
		if err != nil {
			fmt.Println("     ! Failed to load batch file '" + batchfile + "' for '" + cmdline + "'")
			return 0
		}
		defer fb.Close()
	}

	if fb == nil {
		execSingle(cmdline, fl, L)
	} else {
		execBatch(cmdline, fl, fb, L)
	}

	return 0
}

/*
  Run a go-lang regex against a line of output

   match(line, "^foo$")
*/
func LMatch(L *lua.LState) int {
	matches := L.NewTable()
	line := L.ToString(1)  /* get argument */
	restr := L.ToString(2) /* get argument */
	re, ne, err := CompileRegExp(restr)
	if err != nil {
		fmt.Println("- Error compiling regex: " + restr)
		L.Push(lua.LNil)
		return 1
	}
	m := re.FindStringSubmatch(line)
	for i, n := range m {
		matches.RawSetString(ne[i], lua.LString(n))
	}
	if len(m) == 0 {
		L.Push(lua.LNil)
	} else {
		L.Push(matches)
	}
	return 1
}

/*
  Internal function to run a task
*/
func taskRoutine(task string, params *lua.LTable) lua.LValue {
	L := NewVm()
	if err := L.DoFile(TaskPath(task)); err != nil {
		panic(err)
	}
	v := RunVmTask(params, L)
	GlobalWg.Done()
	return v
}

/*
  Run a task concurrently

   asynctask('nikto', params)
*/
func LAsyncTask(L *lua.LState) int {
	task := L.ToString(1)  /* get argument */
	params := L.ToTable(2) /* get argument */
	GlobalWg.Add(1)
	go taskRoutine(task, params)
	return 0
}

/*
  Run a task that needs to complete before progressing

   synctask('nikto', params)
*/
func LSyncTask(L *lua.LState) int {
	task := L.ToString(1)  /* get argument */
	params := L.ToTable(2) /* get argument */
	GlobalWg.Add(1)
	v := taskRoutine(task, params)
	L.Push(v)
	return 1
}
func LMkdir(L *lua.LState) int {
	dirname := L.ToString(1) /* get argument */
	os.MkdirAll(dirname, os.ModePerm)
	return 0
}

/*
  Write a list of addresses to a file

    writelist(tcp['80'])
*/
func LWriteList(L *lua.LState) int {
	list := L.ToTable(1)  /* get argument */
	file := L.ToString(2) /* get argument */
	f, _ := os.Create(file)
	defer f.Close()
	list.ForEach(func(k lua.LValue, v lua.LValue) {
		f.WriteString(v.String() + "\n")
	})
	return 0
}

/*
  Wait for all async tasks to complete

    wait()
*/
func LWait(L *lua.LState) int {
	return 0
}

/*
  Check the async task load

    load()
*/
func LLoad(L *lua.LState) int {
	return 1
}
