package main

import (
	"fmt"
)

func initialScan(args []string) {
	fmt.Println("+ Running initial scan....")
	args = append(args, "-v3")
	nmap := NewScanner("nmap", args)
	nmap.Start()
	for {
		line, err := nmap.ReadLine()
		t := new(Task)
		re, ne, _ := t.CompileRegExp("Discovered open port (?P<port>\\d*)")
		m := re.FindStringSubmatch(line)
		md := map[string]string{}
		for i, n := range m {
			md[ne[i]] = n
		}
		if len(m) > 0 {
			fmt.Println("Port ", md["port"])
		}

		if err != nil {
			break
		}
	}
	fmt.Println("+ Finished initial scan\n+ Waiting on enumeration scans...")
}
