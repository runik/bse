package main

import (
	"fmt"
	"github.com/yuin/gopher-lua"
	"os"
	"path"
	"regexp"
	"sync"
)

var GlobalWg sync.WaitGroup
var GlobalPath string

func main() {
	fmt.Println("[ Braunching Service Enumeration v2.0 ]")
	setGlobalPath()
	L := NewVm()
	t := "entry"

	if len(os.Args) > 2 {
		t = os.Args[2]
	}
	if err := L.DoFile(TaskPath(t)); err != nil {
		panic(err)
	}
	args := L.NewTable()
	args.RawSetString("NETWORK", lua.LString(os.Args[1]))

	RunVmTask(args, L)
	GlobalWg.Wait()
	fmt.Println("[ Complete ]")
}

func setGlobalPath() {
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	dir := path.Dir(ex)
	GlobalPath = dir
}

func TaskPath(task string) string {
	return GlobalPath + "/method/" + task + ".lua"
}

func ParseCommandLine(cmdline string) (string, []string) {
	inargs := false
	inq := ""
	esc := false
	command := ""
	current := ""
	args := []string{}

	fassign := func() {
		if len(current) == 0 {
			return
		}
		if inargs == true {
			args = append(args, current)
		} else {
			command = current
			inargs = true
		}
		current = ""
	}

	fstring := func(q string) {
		if inq == "" {
			inq = q
		} else if inq == q {
			fassign()
			inq = ""
		} else {
			current = current + q
		}
	}

	fesc := func(c rune) {
		switch {
		case c == '"' || c == '\\' || c == '\'' || c == '`':
			current = current + string(c)
			return
		case c == 'n':
			current = current + "\n"
			return
		}
	}
	for _, ch := range cmdline {
		if esc == true {
			fesc(ch)
			esc = false
			continue
		}
		switch {
		case ch == ' ' || ch == '\t':
			if inq != "" {
				current = current + string(ch)
			} else {
				fassign()
			}
			break
		case ch == '"' || ch == '\'' || ch == '`':
			fstring(string(ch))
			break
		case ch == '\\':
			esc = true
			break
		default:
			current = current + string(ch)
		}
	}

	if len(current) > 0 {
		fassign()
	}

	return command, args
}

func CompileRegExp(str string) (*regexp.Regexp, []string, error) {
	re := regexp.MustCompile(str)
	ne := re.SubexpNames()
	return re, ne, nil
}
