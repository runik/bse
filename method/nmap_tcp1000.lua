open = {}
ports = {
   ['21']=1, ['22']=1,  ['23']=1,  ['25']=1,
   ['80']=1, ['443']=1, ['139']=1, ['445']=1
}
function run(params)
   network = params["NETWORK"]
   mkdir("tcp")
   print("   > running nmap tcp syn scan...")
   command("nmap -f --top-ports 50 -v3 -d -sS -Pn -oA scans/tcp1000 " .. network, "verbose/nmap_tcp1000")
   listing = {}
   for p, t in pairs(open) do
      print("     + port " .. p .. ": "..#t)
      table.insert(listing, p)
      for _, a in pairs(t) do
	 writelist(t, "tcp/"..p..".lst")
      end
   end
   return listing
end

function parse(line)
   r = match(line, "^Discovered open port (?P<port>\\d*)/tcp on (?P<address>\\S*)")
   if r == nil then return end
   if ports[ r['port'] ] == nil then return end
   p = r.port
   a = r.address
   if open[p] == nil then
      open[p] = {}
   end
   table.insert(open[p], a)
   print("     % discovered "..p.." on "..a)
end
