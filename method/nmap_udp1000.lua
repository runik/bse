open = {}
ports = { ['53']=1, ['111']=1, ['161']=1 }
function run(params)
   network = params["NETWORK"]
   mkdir("udp")
   print("   > running nmap udp scan...")
   command("nmap -f --top-ports 10 -sU -v3 -d -oA scans/udp1000 " .. network, "verbose/nmap_udp1000")
   listing = {}
   for p, t in pairs(open) do
      print("     + port " .. p .. ": "..#t)
      table.insert(listing, p)
      for _, a in pairs(t) do
	 writelist(t, "udp/"..p..".lst")
      end
   end
   return listing
end

function parse(line)
   r = match(line, "^Discovered open port (?P<port>\\d*)/udp on (?P<address>\\S*)")
   if r == nil then return end
   if ports[ r['port'] ] == nil then return end
   p = r.port
   a = r.address
   if open[p] == nil then
      open[p] = {}
   end
   table.insert(open[p], a)
   print("     % discovered "..p.." on "..a)
end
