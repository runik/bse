numtrace = 0
function run(params)
   NETWORK = params["NETWORK"]
   print("   > Running nmap traceroute...")
   command("nmap -v3 -d --traceroute -sn -oA scans/traceroute " .. NETWORK, "verbose/nmap_traceroute")
   print("     + "..numtrace.." hosts traced")
end

function parse(line)
   r = match(line, "^TRACEROUTE")
   if r == nil then return end
   numtrace = numtrace + 1
end
