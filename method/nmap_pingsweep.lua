NETWORK = ""
active = {}
numactive = 0
function run(params)
   NETWORK = params["NETWORK"]
   print("   > Running nmap ping sweep...")
   command("nmap -d -sn -v3 -oA scans/pingsweep "..NETWORK, "verbose/nmap_pingsweep.out")
   out = {}
   print("     + " .. numactive .. " hosts discovered")
   writelist(active, "scans/pingsweep.lst")
   return out
end

function addhost(addr)
   active[numactive]= addr
   numactive = numactive + 1
end


function parse(line)
   r = match(line, "ping packet back from (?P<address>\\S*):")
   if r == nil then return end
   addhost(r['address'])
end
