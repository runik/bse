NETWORK = ""
portlog = {}
function run(params)
   NETWORK = params["NETWORK"]
   mkdir("verbose")
   print("[+] Running network probes on "..NETWORK.."...")
   mkdir("scans")
   synctask("nmap_traceroute", params)
   synctask("nmap_pingsweep",params)
   tcp = synctask("nmap_tcp1000",params)
   udp = synctask("nmap_udp1000",params)
   mkdir("enum")
   print("[+] Running service enumeration...")
   service_smb(tcp,udp)
   service_http(tcp)
end

function parse(line)
end

function service_smb(tcp, udp)
   asynctask("enum4linux", {['hosts']="tcp/445"})
   asynctask("enum4linux", {['hosts']="tcp/139"})
end

function service_http(tcp)
   asynctask("nikto", {['port']="80"})
end

function service_smtp(tcp)
end





