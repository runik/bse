RHOST = ""
RPORT = ""
function run(params)
   RHOST = params["RHOST"]
   RPORT = params["RPORT"]
   command("nmap -p" ..RPORT..  " --script=ftp-anon -Pn ".. params["RHOST"])
end

function parse(line)
   response = match(line, "ftp-anon: Anonymous FTP login allowed")
   
   if response == nil then return end
   print("#     Anonymous login allowed on "..RPORT.. "!")
end
