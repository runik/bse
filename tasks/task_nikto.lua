RHOST=""
RPORT=""
branches = {
   wordpress = false,
   drupal = false
}

function run(params)
   RHOST = params["RHOST"]
   RPORT = params["RPORT"]
   print("++Nikto Enumeration (" .. RPORT .. ")")
   command("nikto -host " .. RHOST .. " -port " .. RPORT, "nikto"..RPORT)
   print("==Nikto Finished (" .. RPORT .. ")")
end

function bundle_params()
   params = {}
   params["RHOST"] = RHOST
   params["RPORT"] = RPORT
   return params
end


function parse(line)
   if branches.wordpress == false then
      r = match(line, "(?P<WP>/wp\\-|wordpress)")
      if r ~= nil then
	    print("# Wordpress installation discovered")
	    branches.wordpress = true
	    enum_wordpress()
	end
   end

   if branches.drupal == false then
      r = match(line, "(?P<D>Drupal|drupal)")
      if r ~= nil then
	    print("# Drupal installation discovered")
	    branches.drupal = true
	    enum_drupal()
	end
   end

end

function enum_wordpress()
    task("task_wpscan", bundle_params())
    task("task_wp_users", bundle_params())
end

function enum_drupal()
    task("task_drupal_enum", bundle_params())
end
