RHOST=""
RPORT=""
function run(params)
   RHOST = params["RHOST"]
   RPORT = params["RPORT"]
   print("> Attempting to download Drupal changelog for version info (" .. RPORT .. ")")
   command("curl " .. RHOST .. ":" .. RPORT.."/CHANGELOG.txt", "drupal_changelog"..RPORT)
   print("> Enumerating Drupal installation (" .. RPORT .. ")")
   command("nmap " .. RHOST .. " --script=+http-drupal-enum -Pn -p" .. RPORT, "drupal"..RPORT)
   print("> Enumerating Drupal users (" .. RPORT .. ")")
   command("nmap " .. RHOST .. " --script=+http-drupal-enum-users -Pn -p" .. RPORT, "drupal_users"..RPORT)
end

function parse(line) end
