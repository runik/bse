RHOST = ""
RNAME = ""
portlog = {}
function run(params)
   print("++Running initial port scan...")
   RHOST = params["RHOST"]
   RNAME = params["RNAME"]
   command("nmap -v3 -Pn -sV " .. params["RHOST"], "nmap1000")
   print("==Finished initial scan")

   print("++Running full port sweep...")
   command("nmap -A -v3 -Pn -sV -p- " .. params["RHOST"], "nmapfull")
   print("==Finished full port sweep")
end

function parse(line)
   response = match(line, "^Discovered open port (?P<RPORT>\\d*)")
   
   if response == nil then return end
   port = response["RPORT"]
   if portlog[port] == true then return end

   params = {}
   portlog[port] = true
   params["RHOST"] = RHOST
   params["RNAME"] = RNAME
   params["RPORT"] = port
   task("service_enum", params)
end



