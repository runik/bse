RHOST = ""
RPORT = ""
RNAME = ""
function run(params)
   RHOST = params["RHOST"]
   RPORT = params["RPORT"]
   RNAME = params["RNAME"]

   print("> Service on port " .. RPORT .. "")
   command("nmap -sV -Pn -p" .. RPORT .. " " .. RHOST)
end

function parse(line)
   response = match(line, "^\\d*/\\S*\\s*open*\\s*(?P<SERVICE>\\S+)\\s*(?P<NAME>.+)")
   
   if response == nil then return end
   print("# Resolved ".. RPORT ..": "..response.SERVICE.."/" .. response.NAME)
   if response["SERVICE"] == "http" then enum_http()
   elseif response["SERVICE"] == "ftp" then enum_ftp()
   elseif response["SERVICE"] == "netbios-ssn" then enum_netbios()
   elseif response["SERVICE"] == "finger" then enum_finger()
   elseif response["SERVICE"] == "rpcbind" then enum_rpc()
   end
end

function bundle_params()
   params = {}
   params["RHOST"] = RHOST
   params["RPORT"] = RPORT
   params["RNAME"] = RNAME
   return params
end

function enum_http()
   print("> Enumerating HTTP ("..RPORT..")")
   params = bundle_params()
   
   task("task_nikto", params)
end

function enum_ftp()
   print("> Enumerating FTP ("..RPORT..")")
   params = bundle_params()
   
   task("task_ftp_anon", params)
end

function enum_netbios()
   print("> Enumerating NetBIOS ("..RPORT..")")
   params = bundle_params()

   task("task_nbtscan", params)
   if RNAME ~= nil then task("task_smb_list", params) end
   
end

function enum_finger()
   print("> Enumerating Finger ("..RPORT..")")
   params = bundle_params()

   task("task_finger_users", params)
end

function enum_msrpc()
   print("> Enumerating MS-RPC ("..RPORT..")")
   params = bundle_params()

   task("task_msrpc_enum", params)
end

function enum_rpc()
   print("> Enumerating RPC ("..RPORT..")")
   params = bundle_params()

   task("task_rpcinfo", params)
end
