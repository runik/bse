# Branching Service Enumeration ('Madcow')
## Automated command line enumeration

This basic tool can be used to automate service enumeration on hosts. It runs as a master host, initiating and managing a tree of CLI tool scans. Each node (including the root) of the tree is a lua script and each can branch out to other scripts based on results of the scan, providing more enumeration.

The hosted lua engine allows it to be easily extensible, allowing branches into own tools or customise what is already there.

The scripts can also tell the master to log the results of the commands for later reference, and it is recommended that critical scans do this.


### Example Usage

```
[runik@kali] ~/Labs/Net/dartmoor/ % bse 10.15.10.4
++Running initial port scan...
! Logging: 'tail -f nmap1000.scan'
> Service on port 80
> Service on port 21
> Enumerating FTP (21)
> Service on port 22
> Enumerating HTTP (80)
++Nikto Enumeration (80)
! Logging: 'tail -f nikto80.scan'
# Wordpress installation discovered
++WPScan (80)
! Logging: 'tail -f wp80.scan'
> Enumerating wordpress users (80)
! Logging: 'tail -f wp_users80.scan'
==WPScan Finished (80)
==Finished initial scan
++Running full port sweep...
! Logging: 'tail -f nmapfull.scan'
==Nikto Finished (80)
==Finished full port sweep
! Waiting for branches to complete...
[ Complete ]
```

### Example node script
```
RHOST = "" -- Script global variable

-- Required: This will always be called
-- This is the entry point to the script
function run(params)
   print("[+] Running example scan")

   -- RHOST is passed in as a param
   RHOST = params["RHOST"]

   -- builtin: run a command line tool
   command("nmap -v3 -sV " .. params["RHOST"], "nmap1000")
   print("[+] Finished example can")
end

-- Optional: Only called when a command is performed
-- Every line of the command called with 'command()'
-- will be passed back here for checking
function parse(line)

   -- builtin: run a Go regexp against the line
   response = match(line, "^Discovered open port (?P<RPORT>\\d*)")
   
   if response == nil then return end

   params = {}
   params["RHOST"] = RHOST
   params["RPORT"] = response["RPORT"]

   -- builtin: branch out to another script
   task("service_enum", params)
end
```

### Dependencies

- Go 1.8.3 +
- [Gopher Lua](https://github.com/yuin/gopher-lua)

### Building and Config

- Add repository to your Go project tree
   - ```go get -u gitlab.com/runik/bse```
- Get dependency
   - ```go get -u github.com/yuin/gopher-lua```
- The ```task/``` directory needs to be available in the same directory as the executable
   - ```ln -s $GOPATH/src/gitlab.com/runik/bse/tasks $GOPATH/bin```
- Build and install BSE
   - ```go install gitlab.com/runik/bse```
- Adding you $GOPATH/bin directory to your $PATH environment variable will allow you to run it from any dir