package main

import (
	"regexp"
)

type Task struct {
	Scanner *Scanner
}

func NewTask(cmdline string) *Task {
	cmd, args := ParseCommandLine(cmdline)
	t := new(Task)
	t.Scanner = NewScanner(cmd, args)
	return t
}
func (p *Task) CompileRegExp(str string) (*regexp.Regexp, []string, error) {
	re := regexp.MustCompile(str)
	ne := re.SubexpNames()
	return re, ne, nil
}
